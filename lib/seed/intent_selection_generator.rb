# frozen_string_literal: true

# Seed script generator for group-formation draws
class IntentSelectionGenerator < DrawGenerator
  private

  def status
    'intent_selection'
  end
end
